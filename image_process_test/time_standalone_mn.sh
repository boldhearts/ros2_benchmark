#!/usr/bin/env bash

ros2 run v4l2_camera v4l2_camera_node > /dev/null &
ros2 run cm730driver cm730driver_node > /dev/null &
ros2 run cm730controller cm730controller_node > /dev/null &
ros2 run mx_joint_controller mx_joint_controller_node > /dev/null &
ros2 run imu_publisher imu_publisher_node > /dev/null &
ros2 run imu_fusion_madgwick imu_fusion_madgwick_node > /dev/null &

ros2 run image_process_test image_process_test_node | grep 'dt:' | cut -f4 -d' ' | tee dt-standalone-mn-$(hostname)-$(date -Iminute).txt | while read -r line ; do ((a++)) ; echo -ne "\r$a" ; done
