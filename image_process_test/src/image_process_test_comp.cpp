// Copyright 2019 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "image_process_test/image_process_test.hpp"
#include <v4l2_camera/v4l2_camera.hpp>

#include <rclcpp/rclcpp.hpp>
#include <memory>

int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);

  rclcpp::executors::SingleThreadedExecutor exec{};

  auto cameraNode = std::make_shared<v4l2_camera::V4L2Camera>(
    rclcpp::NodeOptions{}.use_intra_process_comms(true));
  auto processNode = std::make_shared<image_process_test::ImageProcessTest>();

  exec.add_node(cameraNode);
  exec.add_node(processNode);

  exec.spin();

  rclcpp::shutdown();
  cameraNode = nullptr;
  processNode = nullptr;

  return 0;
}
