// Copyright 2019 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "image_process_test/image_process_test.hpp"

#include <cv_bridge/cv_bridge.h>
#include <opencv2/imgproc.hpp>

#include <string>

namespace image_process_test
{

ImageProcessTest::ImageProcessTest()
: rclcpp::Node{"image_process_test", rclcpp::NodeOptions{}.use_intra_process_comms(true)}
{
  // static sampling periodin seconds
  get_parameter_or("use_multi_node", use_multi_node_, false);

  RCLCPP_INFO_EXPRESSION(get_logger(), use_multi_node_, "Use multi-node example.");

  RCLCPP_INFO(get_logger(), "Create subscriber for the topic `/image_raw`");

  image_sub_ = create_subscription<sensor_msgs::msg::Image>(
    "/image_raw",
    10,
    [this](sensor_msgs::msg::Image::UniquePtr img) {
      std::stringstream ss;
      ss << "Received image, message address:\t" << img.get();
      RCLCPP_INFO(get_logger(), ss.str());

      ss.str("");
      ss << "Data address:\t" << reinterpret_cast<uintptr_t>(img->data.data());
      RCLCPP_INFO(get_logger(), ss.str());

      auto sharedImg = std::shared_ptr<sensor_msgs::msg::Image>{std::move(img)};
      auto cvPtr = cv_bridge::toCvShare(sharedImg, sensor_msgs::image_encodings::RGB8);

      ss.str("");
      ss << "Shared data address:\t" << reinterpret_cast<uintptr_t>(sharedImg->data.data());
      RCLCPP_INFO(get_logger(), ss.str());

      ss.str("");
      ss << "OpenCV data address:\t" << reinterpret_cast<uintptr_t>(cvPtr->image.data);
      RCLCPP_INFO(get_logger(), ss.str());

      RCLCPP_INFO(get_logger(),
      std::to_string(cvPtr->image.cols) + "x" + std::to_string(cvPtr->image.rows));

      auto out = cv::Mat{};
      cv::Sobel(cvPtr->image, out, CV_16S, 1, 0);
      auto t = now();
      auto diff = t - sharedImg->header.stamp;
      RCLCPP_INFO(get_logger(), std::string{"dt: "} + std::to_string(diff.nanoseconds()));
    });

  if (use_multi_node_) {

    RCLCPP_INFO(get_logger(), "Create subscriber for the topic `/imu/data`");
    imu_sub_ = create_subscription<sensor_msgs::msg::Imu>(
      "/imu/data",
      10,
      [ = ](sensor_msgs::msg::Imu::SharedPtr imuMsg) {

        RCLCPP_DEBUG(get_logger(), std::string{"imu w: "} + std::to_string(imuMsg->orientation.w));
        auto t = now();
        auto diff = t - imuMsg->header.stamp;
        RCLCPP_INFO(get_logger(), std::string{"imu dt: "} + std::to_string(diff.nanoseconds()));
      });

    RCLCPP_INFO(get_logger(), "Create subscriber for the topic `/joint_states`");
    js_sub_ = create_subscription<sensor_msgs::msg::JointState>(
      "/joint_states",
      10,
      [ = ](sensor_msgs::msg::JointState::SharedPtr jsMsg) {

        RCLCPP_DEBUG(get_logger(), std::string{"js pos2: "} + std::to_string(jsMsg->position.at(
          2)));
        auto t = now();
        auto diff = t - jsMsg->header.stamp;
        RCLCPP_INFO(get_logger(), std::string{"js dt: "} + std::to_string(diff.nanoseconds()));
      });
  }
}

ImageProcessTest::~ImageProcessTest()
{
}

}  // namespace image_process_test
