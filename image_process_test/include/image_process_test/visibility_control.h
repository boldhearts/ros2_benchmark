// Copyright 2019 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef IMAGE_PROCESS_TEST__VISIBILITY_CONTROL_H_
#define IMAGE_PROCESS_TEST__VISIBILITY_CONTROL_H_

// This logic was borrowed (then namespaced) from the examples on the gcc wiki:
//     https://gcc.gnu.org/wiki/Visibility

#if defined _WIN32 || defined __CYGWIN__
  #ifdef __GNUC__
    #define IMAGE_PROCESS_TEST_EXPORT __attribute__ ((dllexport))
    #define IMAGE_PROCESS_TEST_IMPORT __attribute__ ((dllimport))
  #else
    #define IMAGE_PROCESS_TEST_EXPORT __declspec(dllexport)
    #define IMAGE_PROCESS_TEST_IMPORT __declspec(dllimport)
  #endif
  #ifdef IMAGE_PROCESS_TEST_BUILDING_LIBRARY
    #define IMAGE_PROCESS_TEST_PUBLIC IMAGE_PROCESS_TEST_EXPORT
  #else
    #define IMAGE_PROCESS_TEST_PUBLIC IMAGE_PROCESS_TEST_IMPORT
  #endif
  #define IMAGE_PROCESS_TEST_PUBLIC_TYPE IMAGE_PROCESS_TEST_PUBLIC
  #define IMAGE_PROCESS_TEST_LOCAL
#else
  #define IMAGE_PROCESS_TEST_EXPORT __attribute__ ((visibility("default")))
  #define IMAGE_PROCESS_TEST_IMPORT
  #if __GNUC__ >= 4
    #define IMAGE_PROCESS_TEST_PUBLIC __attribute__ ((visibility("default")))
    #define IMAGE_PROCESS_TEST_LOCAL  __attribute__ ((visibility("hidden")))
  #else
    #define IMAGE_PROCESS_TEST_PUBLIC
    #define IMAGE_PROCESS_TEST_LOCAL
  #endif
  #define IMAGE_PROCESS_TEST_PUBLIC_TYPE
#endif

#endif  // IMAGE_PROCESS_TEST__VISIBILITY_CONTROL_H_
