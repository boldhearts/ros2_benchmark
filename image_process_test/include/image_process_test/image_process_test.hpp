// Copyright 2019 Bold Hearts
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef IMAGE_PROCESS_TEST__IMAGE_PROCESS_TEST_HPP_
#define IMAGE_PROCESS_TEST__IMAGE_PROCESS_TEST_HPP_

#include <image_transport/image_transport.h>
#include <sensor_msgs/msg/imu.hpp>
#include <sensor_msgs/msg/joint_state.hpp>

#include <rclcpp/rclcpp.hpp>

#include "image_process_test/visibility_control.h"

namespace image_process_test
{

class ImageProcessTest : public rclcpp::Node
{
public:
  ImageProcessTest();

  virtual ~ImageProcessTest();

private:
  bool use_multi_node_;
  rclcpp::Subscription<sensor_msgs::msg::Image>::SharedPtr image_sub_;
  //image_transport::Subscriber image_sub_;

  rclcpp::Subscription<sensor_msgs::msg::Imu>::SharedPtr imu_sub_;
  rclcpp::Subscription<sensor_msgs::msg::JointState>::SharedPtr js_sub_;

};

}  // namespace image_process_test

#endif  // IMAGE_PROCESS_TEST__IMAGE_PROCESS_TEST_HPP_
