#!/usr/bin/env bash

ros2 run image_process_test image_process_test_comp_mn_noipc __params:=comp-mn-params.yml | grep 'dt:' | cut -f4 -d' ' | tee dt-comp-mn-noipc-$(hostname)-$(date -Iminute).txt | while read -r line ; do ((a++)) ; echo -ne "\r$a" ; done
