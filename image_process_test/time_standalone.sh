#!/usr/bin/env bash

ros2 run v4l2_camera v4l2_camera_node &> /dev/null &

ros2 run image_process_test image_process_test_node | grep 'dt:' | cut -f4 -d' ' | tee dt-standalone-$(hostname)-$(date -Iminute).txt | while read -r line ; do ((a++)) ; echo -ne "\r$a" ; done
