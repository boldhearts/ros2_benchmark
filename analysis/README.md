# ROS 2 Node Composition Benchmark Analysis

The benchmark analysis is done in a Jupyter notebook. To be able to
run it, install the provided requirements (a Python 3 virtual
environment is recommended):

    pip install -r requirements.txt

Then launch jupyter notebook and browse to the `.ipynb` file:

    jupyter notebook
